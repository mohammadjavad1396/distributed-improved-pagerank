import sys
from operator import add

from pyspark.sql import SparkSession

spark = SparkSession.builder.appName('q4_session').getOrCreate()
sc = spark.sparkContext

try:
    file = sys.argv[1]
    alpha = float(sys.argv[2])
    iteration = int(sys.argv[3])
    threshold = float(sys.argv[4])
except:
    print('arguments are not valid')
    sys.exit()

print('arguments are correct')
debug = False

lines = sc.textFile(file) \
    .map(lambda x: x.split()) \
    .filter(lambda x: x[0] != x[1]) \
    .repartition(8)

nodes = lines.flatMap(lambda xs: [int(x) for x in xs[:2]]) \
    .distinct() \
    .repartition(8)

num_nodes = nodes.count()
nodes_action = nodes.collect()

nodes_with_outlink_weights_ = lines \
    .map(lambda x: (int(x[0]), (int(x[1]), float(x[2])))) \
    .groupByKey() \
    .mapValues(list) \
    .repartition(8)

if debug:
    print(nodes_with_outlink_weights_.collect())

nodes_with_outlink_weights_ = nodes_with_outlink_weights_ \
    .map(lambda x: (x[0], [(k, v / sum([v[1] for v in x[1]]) * (1 - alpha)) for k, v in x[1]])) \
    .repartition(8)

if debug:
    print(nodes_with_outlink_weights_.collect())

nodes_without_outlink = nodes.subtract(nodes_with_outlink_weights_.keys()) \
    .map(lambda x: (x, [(node, 1 / (num_nodes - 1) * (1 - alpha))
                        for node in nodes_action if node != x])) \
    .repartition(8)

if debug:
    print(nodes_without_outlink.collect())

S_matrix = nodes_with_outlink_weights_ \
    .union(nodes_without_outlink) \
    .repartition(8) \
    .cache()

if debug:
    print(S_matrix.collect())

pi_matrix = sc.parallelize([(node, 1 / num_nodes) for node in nodes_action]) \
    .repartition(8) \
    .cache()

if debug:
    print(pi_matrix.collect())

while iteration > 0:

    old_pi_matrix = pi_matrix
    temp_S_matrix = S_matrix

    temp_S_matrix = temp_S_matrix.join(old_pi_matrix) \
        .map(lambda x: (x[0], [(k, v * x[1][-1]) for idx, (k, v) in enumerate(x[1][0])])) \
        .repartition(8) \
        .cache()

    if debug:
        print(temp_S_matrix.collect())

    # sum_pi_matrix_action = old_pi_matrix.map(lambda x: x[1]).sum()
    # it could be alpha * sum_pi_matrix_action / num_nodes but we know
    # the summation in this case would be 1 so we are going to not collect it
    random_jump_sum = alpha / num_nodes

    pi_matrix = temp_S_matrix.values() \
        .flatMap(lambda x: x) \
        .reduceByKey(add) \
        .map(lambda x: (x[0], x[1] + random_jump_sum))

    if debug:
        print(pi_matrix.collect(), 'pi_matrix_collect')

    delta = pi_matrix.join(old_pi_matrix).mapValues(lambda x: abs(x[0] - x[1])).values()
    delta_action = delta.sum()

    if debug:
        print(delta.collect(), 'limit_action')

    if delta_action < threshold:
        break

    iteration -= 1
    print(iteration)

pi_matrix.saveAsTextFile('./results')
